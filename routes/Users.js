const express = require('express')
const users = express.Router()
const cors = require('cors')
const jwt = require('jsonwebtoken')
const Product = require('../models/Product')
const User = require('../models/User')

users.use(cors())

process.env.SECRET_KEY = 'secret'

users.get('/products', (req, res) => {

  jwt.verify(req.headers['authorization'], process.env.SECRET_KEY)

  Product.findAll({})
    .then(product => {
      if (product) {
        res.json(product)
      } else {
        res.send('Product does not exists!')
      }
    })
    .catch(err => {
      res.send('error: ' + err)
    })
})

users.get('/products/:id', (req, res) => {

  jwt.verify(req.headers['authorization'], process.env.SECRET_KEY)

  Product.findOne({
      where: {
        id: req.params.id
      }
    })
    .then(product => {
      if (product) {
        res.json(product)
      } else {
        res.send('Product does not exists!')
      }
    })
    .catch(err => {
      res.send('error: ' + err)
    })
})

users.delete('/products/:id', (req, res) => {

  jwt.verify(req.headers['authorization'], process.env.SECRET_KEY)

  Product.destroy({
      where: {
        id: req.params.id
      }
    })
    .then(product => {
      if (product) {
        res.json(product)
      } else {
        res.send('Product does not exists!')
      }
    })
    .catch(err => {
      res.send('error: ' + err)
    })
});

users.put('/products/:id', (req, res) => {

  jwt.verify(req.headers['authorization'], process.env.SECRET_KEY)

  Product.update({
    "name": req.body.name,
    "value": req.body.value,
    "category": req.body.category,
    "description": req.body.description,
    "quantity": req.body.quantity,
    }, {
      where: {
        "id": req.params.id
      }
    })
    .then(product => {
      if (product) {
        res.json(product)
      } else {
        res.send('Product does not exists!')
      }
    })
    .catch(err => {
      res.send('error: ' + err)
    })
});


users.post('/products/', (req, res) => {

  jwt.verify(req.headers['authorization'], process.env.SECRET_KEY)
  const today = new Date()

  Product.create({
      "name": req.body.name,
      "value": req.body.value,
      "category": req.body.category,
      "description": req.body.description,
      "quantity": req.body.quantity,
      "created": today
    })
    .then(product => {
      if (product) {
        res.json(product)
      } else {
        res.send('Product already exists!')
      }
    })
    .catch(err => {
      res.send('error: ' + err)
    })
});

users.post('/register', (req, res) => {
  const today = new Date()
  const userData = {
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email,
    password: req.body.password,
    created: today
  }

  User.findOne({
    where: {
      email: req.body.email
    }
  })
    .then(user => {
      if (!user) {
        User.create(userData)
          .then(user => {
            let token = jwt.sign(user.dataValues, process.env.SECRET_KEY, {
              expiresIn: 1440
            })
            res.json({ token: token })
          })
          .catch(err => {
            res.send('error: ' + err)
          })
      } else {
        res.json({ error: 'User already exists' })
      }
    })
    .catch(err => {
      res.send('error: ' + err)
    })
});

users.post('/login', (req, res) => {

  User.findOne({
      where: {
        email: req.body.email,
        password: req.body.password
      }
    })
    .then(user => {
      if (user) {
        let token = jwt.sign(user.dataValues, process.env.SECRET_KEY, {
          expiresIn: 1440
        })
        res.json({
          token: token
        })
      } else {
        res.send('Invalid Login or Password!')
      }
    })
    .catch(err => {
      res.send('error: ' + err)
    })

})


users.get('/profile', (req, res) => {

  var decoded = jwt.verify(req.headers['authorization'], process.env.SECRET_KEY)

  User.findOne({
      where: {
        id: decoded.id
      }
    })
    .then(user => {
      if (user) {
        res.json(user)
      } else {
        res.send('User does not exists!')
      }
    })
    .catch(err => {
      res.send('error: ' + err)
    })
})

module.exports = users