import { Component } from "@angular/core";
import { AuthenticationService, TokenPayload, Products } from "../authentication.service";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  templateUrl: "./products-edit.component.html",
  styleUrls: ["./products-edit.component.css"],
})
export class ProductsEditComponent {

  details: Products;

  registerForm: FormGroup;
  submitted = false;

  credentials: Products = {
    // @ts-ignore
    id: this.route.params.value.id,
    Id: 0,
    name: '',
    value: 0,
    category: '',
    manufacturer: '',
    description: '',
    created: '',
    }
  
  
  constructor(private auth: AuthenticationService, private router: Router, private route: ActivatedRoute, private formBuilder: FormBuilder) {}

  productsAlter(credentials) {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
       return;
    }
    this.auth.productsAlter(this.credentials).subscribe(
      () => {
        //console.log(this.credentials)
        alert("Changes Saved!")
       this.router.navigateByUrl("/products");
      },
      err => {
        console.error(err);
      }
    );
  }

  get f() { return this.registerForm.controls; }
  
  ngOnInit() {
    window.scrollTo(0, 0)
    // @ts-ignore
    this.auth.productsDetails(this.route.params.value.id).subscribe(
      product => {
       this.credentials = product
        this.registerForm = this.formBuilder.group({
          name: [this.credentials.name, [Validators.required, Validators.minLength(5)]],
          value: [this.credentials.value, [Validators.required, Validators.minLength(5)]],
          category: [this.credentials.category,[Validators.required, Validators.minLength(5)]],
          description: [this.credentials.description, [Validators.required, Validators.minLength(10)]],
          manufacturer: [this.credentials.manufacturer, [Validators.required, Validators.minLength(5)]],
        });
      },
      err => {
        console.error(err)
      }
    )
  }
}
