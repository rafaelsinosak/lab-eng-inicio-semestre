import { Component } from '@angular/core';
import { AuthenticationService, Products } from '../authentication.service';
import { ActivatedRoute } from "@angular/router";


@Component({
  templateUrl: './products-details.component.html'
})
export class ProductsDetailsComponent {
  details: Products

  constructor(private auth: AuthenticationService, private route: ActivatedRoute) {}

  ngOnInit() {
    window.scrollTo(0, 0)
    // @ts-ignore
    this.auth.productsDetails(this.route.params.value.id).subscribe(
      product => {
        this.details = product
        //console.log(this.details)
      },
      err => {
        console.error(err)
      }
    )
  }
}
