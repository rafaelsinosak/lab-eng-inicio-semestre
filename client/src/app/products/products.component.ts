import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { AuthenticationService, Products } from '../authentication.service'
import 'rxjs';
// @ts-ignore
import { Subject } from 'rxjs';


import { DataTableDirective } from 'angular-datatables';

@Component({
  templateUrl: './products.component.html',
  styleUrls: ["./products.component.css"],
})
export class ProductsComponent implements OnDestroy, OnInit {
  datatableElement: DataTableDirective;
  
  details: Products [] = [];
   // @ts-ignore
  dtTrigger: Subject = new Subject();

  constructor(private auth: AuthenticationService) {}

  @ViewChild(DataTableDirective)
  dtOptions: DataTables.Settings = {};

  delete(id, fornecedor) {
    if(confirm("Delete: "+fornecedor)) {

      this.auth.delete(id).subscribe(result => {
        this.ngOnDestroy()
        this.details = this.details.filter((elem) => {
          return elem.id !== id;
      });
        this.ngOnInit();
      }, error => console.log('Error: ', error));
    }
}

ngOnInit() {
  window.scrollTo(0, 0)
  this.dtOptions = {
    "dom": 'Blfrtip',
    // @ts-ignore
    initComplete:  function (settings, json) {
      // @ts-ignore
      $('button').removeClass('dt-button');
   },
   // @ts-ignore
   "buttons": [{
      "text": "Copy Table",
      "className": 'btn btn-dark',
      "extend": "copy",
      "exportOptions": {
        "columns": [0 ,1 ,2 ,3 ,4 ]
    }
     },{
    "text": "Download Excel",
    "className": 'btn btn-dark',
    "extend": "excel",
    "exportOptions": {
      "columns": [0 ,1 ,2 ,3 ,4 ]
  }
   },{
    "text": "Download CSV",
    "className": 'btn btn-dark',
    "extend": "csv",
    "exportOptions": {
      "columns": [0 ,1 ,2 ,3 ,4 ]
  }
   }],
    "responsive":true,
    "columnDefs": [  {
    "targets": [7],
                "orderable": false,
                "searchable": false,
  }],
    "lengthMenu": [
      [20, 40, 60, 80],
      [20, 40, 60, 80]
  ],
  };

  this.auth.productsAll().subscribe(
    product => {
      this.details = product
      console.log(this.details)
      // @ts-ignore
      this.dtTrigger.next();
    },
    err => {
      console.error(err)
    }
  )

}

  ngOnDestroy(): void {
    // @ts-ignore
    this.dtTrigger.unsubscribe();
  }

}