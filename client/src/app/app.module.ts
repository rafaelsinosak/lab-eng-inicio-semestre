import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component'
import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

import { ProductsComponent } from './products/products.component';
import { ProductsDetailsComponent } from './products-details/products-details.component';
import { ProductsCreateComponent } from './products-create/products-create.component';
import { ProductsEditComponent } from './products-edit/products-edit.component';

import {NgxMaskModule} from 'ngx-mask'

import { ReactiveFormsModule } from '@angular/forms';

import { TooltipModule } from 'ng2-tooltip-directive';

import { HomeComponent } from './home/home.component';
import { AuthenticationService } from './authentication.service';
import { AuthGuardService } from './auth-guard.service';

import { ChartModule, HIGHCHARTS_MODULES  } from 'angular-highcharts';
import * as more from 'highcharts/highcharts-more.src';
import * as exporting from 'highcharts/modules/exporting.src';

import { DataTablesModule } from 'angular-datatables';

const routes: Routes = [
  { path: '',
    component: HomeComponent
  },
  { path: 'login',
    component: LoginComponent
  },
  { path: 'register',
    component: RegisterComponent
  },
  { 
    path: 'products',
    component: ProductsComponent,
   canActivate: [AuthGuardService]
   },
   { 
    path: 'products-details/:id',
    component: ProductsDetailsComponent,
    canActivate: [AuthGuardService]
   },
   { 
    path: 'products-edit/:id',
    component: ProductsEditComponent,
    canActivate: [AuthGuardService]
   },
   { 
    path: 'products-create',
    component: ProductsCreateComponent,
    canActivate: [AuthGuardService]
   },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuardService]
  }
]

// @ts-ignore
export const options: Partial<IConfig> | (() => Partial<IConfig>);

@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent,
    LoginComponent,
    RegisterComponent,
    ProductsDetailsComponent,
    ProductsComponent,
    ProductsCreateComponent,
    ProductsEditComponent,
    HomeComponent
  ],
  imports: [
    NgxMaskModule.forRoot(options),
    ReactiveFormsModule,
    ChartModule,
    DataTablesModule,
    BrowserModule,
    FormsModule,
    TooltipModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
  ],
  exports: [ RouterModule ],
  providers: [AuthenticationService,
     AuthGuardService,
     { provide: HIGHCHARTS_MODULES, useFactory: () => [ more, exporting ] } 
    ],
  bootstrap: [AppComponent]
})
export class AppModule {}
