import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders   } from '@angular/common/http'
import { Observable, of } from 'rxjs'
import { map } from 'rxjs/operators'
import { Router } from '@angular/router'

export interface UserDetails {
  id: number;
  first_name: string;
  last_name: string;
  email: string;
  password: string;
  exp: number;
  iat: number;
}

export interface Products {
  Id: any;
  id: number;
  name: string;
  value: number;
  category: string;
  description: string;
  manufacturer: string;
  created: string;
}

interface TokenResponse {
  token: string;
}

export interface TokenPayload {
  id: number;
  first_name: string;
  last_name: string;
  email: string;
  password: string;
}

@Injectable()
export class AuthenticationService {
  private token: string;

  constructor(private http: HttpClient, private router: Router) {}

  private saveToken(token: string): void {
    localStorage.setItem('usertoken', token);
    this.token = token;
  }

  private getToken(): string {
    if (!this.token) {
      this.token = localStorage.getItem('usertoken')
    }
    return this.token;
  }

  public getUserDetails(): UserDetails {
    const token = this.getToken()
    let payload;
    if (token) {
      payload = token.split('.')[1];
      payload = window.atob(payload);
      return JSON.parse(payload);
    } else {
      return null;
    }
  }

  public getFornecedores(): Products {
    const token = this.getToken();
    let payload;
    if (token) {
      payload = token.split('.')[1];
      payload = window.atob(payload);
      return JSON.parse(payload);
    } else {
      return null;
    }
  }

  public isLoggedIn(): boolean {
    const user = this.getUserDetails()
    if (user) {
      return user.exp > Date.now() / 1000;
    } else {
      return false;
    }
  }

  public login(user: TokenPayload): Observable<any> {
    const base = this.http.post(`http://localhost:3000/users/login`, user);

    const request = base.pipe(
      map((data: TokenResponse) => {
        if (data.token) {
          this.saveToken(data.token);
        }
        return data;
      })
    )
    return request;
  }

  public profile(): Observable<any> {
    // SUBSTITUIR http://localhost por localhost em PRODUCAO
    return this.http.get(`http://localhost:3000/users/profile`, {
      headers: { Authorization: ` ${this.getToken()}` }
    });
  }
  
  public register(user: TokenPayload): Observable<any> {
    return this.http.post(`http://localhost:3000/users/register`, user);
  }

  public productsCreate(user: Products): Observable<any>{
    return this.http.post('http://localhost:3000/users/products/', user,
    {
      headers: { Authorization: ` ${this.getToken()}` }
    });
  }

  public productsCreateHome(user: Products): Observable<any>{
    return this.http.post('http://localhost:3000/users/products/', user);
  }
    
  public productsAlter(user: Products): Observable<any>{
    return this.http.put('http://localhost:3000/users/products/' + (user.Id), user, {
      headers: { Authorization: ` ${this.getToken()}` }
    });
  }

  public productsDetails(id): Observable<any> {
    return this.http.get(`http://localhost:3000/users/products/` + id, {
      headers: { Authorization: ` ${this.getToken()}` }
    });
  }

  public productsAll(): Observable<any> {
    return this.http.get(`http://localhost:3000/users/products/`, {
      headers: { Authorization: ` ${this.getToken()}` }
    });
  }

  public delete(id): Observable<any>{
    return this.http.delete('http://localhost:3000/users/products/' + id, {
      headers: { Authorization: ` ${this.getToken()}` }
    });
}
  public logout(): void {
    this.token = '';
    window.localStorage.removeItem('usertoken');
    this.router.navigateByUrl('/');
  }
}
