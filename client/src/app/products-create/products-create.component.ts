import { Component, OnInit } from "@angular/core";
import { AuthenticationService, TokenPayload, Products } from "../authentication.service";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  templateUrl: "./products-create.component.html",
  styleUrls: ["./products-create.component.css"],
})
export class ProductsCreateComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;

  credentials: Products = {
    Id: 0,
    id: 0,
    name: '',
    value: 0,
    category: '',
    manufacturer: '',
    description: '',
    created: '',
  }

  details: ProductsCreateComponent [] = [];

  constructor(private auth: AuthenticationService, private router: Router, private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: [this.credentials.name, [Validators.required, Validators.minLength(5)]],
          value: [this.credentials.value, [Validators.required, Validators.minLength(5)]],
          category: [this.credentials.category, [Validators.required, Validators.minLength(5)]],
          description: [this.credentials.description, [Validators.required, Validators.minLength(10)]],
          manufacturer: [this.credentials.manufacturer, [Validators.required, Validators.minLength(5)]],
    });
}

get f() { return this.registerForm.controls; }


 // @ts-ignore
productsCreate(test) {
  this.submitted = true;

  // stop here if form is invalid
  if (this.registerForm.invalid) {
     return;
  }


    this.auth.productsCreate(this.credentials).subscribe(
      () => {
        alert("Fornecedor cadastrado com Sucesso!")
       this.router.navigateByUrl("/products");
      },
      err => {
        console.error(err);
      }
    );
  
  
}

}
